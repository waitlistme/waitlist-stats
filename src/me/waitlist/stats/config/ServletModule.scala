package me.waitlist.stats
package config

import scala.collection.JavaConversions.mapAsJavaMap
import com.googlecode.objectify.ObjectifyFilter
import com.sun.jersey.api.core.PackagesResourceConfig
import com.sun.jersey.api.core.ResourceConfig
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer
import me.waitlist.stats.http.HourlyServlet
import me.waitlist.stats.http.StatsServlet

/**
 * Servlet configuration for establishing filters and servlets (e.g., RESTful endpoints).
 *
 * @author dsumera
 */
class ServletModule extends me.waitlist.core.config.ServletModule {

  override def configureServlets {

    super.configureServlets

    // crons

    // perform all stats-based updates
    serve("/cron/stats/*") `with` classOf[StatsServlet]

    // tasks

    // update hourly
    serve("/task/update/hourly") `with` classOf[HourlyServlet]
  }
}