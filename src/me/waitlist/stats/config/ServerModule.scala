package me.waitlist.stats
package config

import com.google.inject.name.Names

/**
 * Bindings for <code>stats</code> module.
 *
 * @author dsumera
 */
class ServerModule extends me.waitlist.core.config.ServerModule {

  override def configure {

    // invoke all standard configurations
    super.configure

    // stats input shard count (this should match the max concurrent value for "stats" queue)
    bind(classOf[Int]) annotatedWith Names.named("stats_shards") toInstance 10

    // stats reducer count
    bind(classOf[Int]) annotatedWith Names.named("stats_reducers") toInstance 5

    // stats processing offset in hours (i.e., time before current time)
    // allows sufficient lag before computing hourly stats (which may change frequently)
    // can set processing frequency to multiple of this number of hours
    bind(classOf[Int]) annotatedWith Names.named("stats_offset") toInstance -5

    // work queue
    bind(classOf[String]) annotatedWith Names.named("queue") toInstance "stats"

    // google cloud storage bucket
    // prod: wlm_stats
    // staging: wlm_stats_staging
    bind(classOf[String]) annotatedWith Names.named("bucket") toInstance "wlm_stats"
  }
}