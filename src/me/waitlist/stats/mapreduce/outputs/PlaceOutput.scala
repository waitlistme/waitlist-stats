package me.waitlist.stats
package mapreduce.outputs

import java.util

import com.google.appengine.tools.mapreduce.Output
import com.google.appengine.tools.mapreduce.OutputWriter
import com.google.common.collect.ImmutableList

import me.waitlist.stats.listener.ContextListener
import me.waitlist.core.dao.PlaceDao
import me.waitlist.core.db.Place

/**
 * Output for <code>Place</code> <code>Stats</code>-based entities.
 *
 * @author dsumera
 */
class PlaceOutput extends Output[Place, Void] {

  /**
   * Create the writers wrt number of shards.
   * @param numShards
   * @return
   */
  override def createWriters(numShards: Int): util.List[_ <: OutputWriter[Place]] = {
    val out: ImmutableList.Builder[PlaceOutputWriter] = ImmutableList.builder()
    for (_ <- 0 until numShards) out.add(new PlaceOutputWriter)
    out.build
  }

  /**
   * Usually returns a pointer to the result generated.
   * @param writers
   * @return
   */
  override def finish(writers: util.Collection[_ <: OutputWriter[Place]]): Void = null

  /**
   * <code>OutputWriter</code> for capturing <code>Stats</code> objects to the datastore.
   */
  private final class PlaceOutputWriter extends OutputWriter[Place] {

    @transient private[this] var placeDao: PlaceDao = _

    override def beginSlice {
      // initialize daos
      placeDao = ContextListener.injector.getInstance(classOf[PlaceDao])
      super.beginSlice
    }

    override def write(place: Place) = placeDao.transact { dao =>
      // retrieve place entity based off id
      dao.find(place.id: Long) match {
        case None => // place not found, continue

        // update place count values with container values and save
        case Some(p) =>
          // diff check to see if we can save a Place write
          if ((p.seated_last_day != place.seated_last_day) ||
            (p.seated_last_week != place.seated_last_week) ||
            (p.seated_last_month != place.seated_last_month) ||
            (p.total_seated != place.total_seated)) {
            p.seated_last_day = place.seated_last_day
            p.seated_last_week = place.seated_last_week
            p.seated_last_month = place.seated_last_month
            p.total_seated = place.total_seated
            dao.persist(p)
          }
      }
    }
  }
}