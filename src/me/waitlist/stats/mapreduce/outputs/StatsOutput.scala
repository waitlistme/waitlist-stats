package me.waitlist.stats
package mapreduce.outputs

import java.util

import com.google.appengine.tools.mapreduce.Output
import com.google.appengine.tools.mapreduce.OutputWriter
import com.google.common.collect.ImmutableList

import me.waitlist.core.dao.DailyStatsDao
import me.waitlist.core.dao.HourlyStatsDao
import me.waitlist.core.dao.OverallStatsDao
import me.waitlist.core.db.HourlyStats
import me.waitlist.core.traits.Dateable
import me.waitlist.stats.listener.ContextListener

/**
 * Output for <code>Stats</code>-based entities.
 *
 * @author dsumera
 */
class StatsOutput extends Output[HourlyStats, Void] with Dateable {

  /**
   * Create the writers wrt number of shards.
   * @param numShards
   * @return
   */
  override def createWriters(numShards: Int): util.List[_ <: OutputWriter[HourlyStats]] = {
    val out: ImmutableList.Builder[StatsOutputWriter] = ImmutableList.builder()
    for (_ <- 0 until numShards) out.add(new StatsOutputWriter)
    out.build
  }

  /**
   * Usually returns a pointer to the result generated.
   * @param writers
   * @return
   */
  override def finish(writers: util.Collection[_ <: OutputWriter[HourlyStats]]): Void = null

  /**
   * <code>OutputWriter</code> for capturing <code>Stats</code> objects to the datastore.
   */
  private final class StatsOutputWriter extends OutputWriter[HourlyStats] {

    @transient private[this] var dlyDao: DailyStatsDao = _
    @transient private[this] var hrDao: HourlyStatsDao = _
    @transient private[this] var ovrDao: OverallStatsDao = _

    override def beginSlice {
      // initialize daos
      dlyDao = ContextListener.injector.getInstance(classOf[DailyStatsDao])
      hrDao = ContextListener.injector.getInstance(classOf[HourlyStatsDao])
      ovrDao = ContextListener.injector.getInstance(classOf[OverallStatsDao])
      super.beginSlice
    }

    override def write(stat: HourlyStats) {
      // split the stat id to get the entity type and actual id
      val (entity :: id :: _) = stat.id.split(":").toList
      
      entity match {
        // save daily stat
        case "d" => dlyDao.transact { _ =>
          val daily = dlyDao.findOrCreate(id)

          // daily stats are based off the "NOW" time, this is based off the stat date (use the year/month/day of the id, GMT)
          val (name :: dt :: _) = id.split("\\|").toList

          // update the date
          val fmt = getSimpleFormat
          fmt.setTimeZone(Dateable.UTC)
          daily.date = fmt parse dt

          // update the place
          if ("place-all" != name)
            daily.place = stat.place

          // update the stat
          daily.merge(stat)
          dlyDao.persist(daily)
        }

        // save hourly overall stat
        case "h" => hrDao.transact { _ =>
          // only hourly is cumulative, based off the stat date
          val hourly = hrDao.findOrCreate(id)
          hourly.date = stat.date

          hourly.merge(stat)
          hrDao.persist(hourly)
        }

        // save overall stat
        case "o" => ovrDao.transact { _ =>
          // place overall stat uses latest stat date
          val overall = ovrDao.findOrCreate(id)

          // retrieve the name of the stat
          val (name :: _) = id.split("\\|").toList

          // update the place and last updated
          if ("place-all" != name) {
            overall.place = stat.place
            overall.last_updated = stat.date
          }

          // update the stat
          overall.merge(stat)
          ovrDao.persist(overall)
        }
      }
    }
  }
}