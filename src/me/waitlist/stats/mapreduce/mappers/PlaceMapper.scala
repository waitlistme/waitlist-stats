package me.waitlist.stats
package mapreduce.mappers

import java.io.Serializable
import java.util.Calendar
import java.util.logging.Logger

import com.google.appengine.api.datastore.Key
import com.google.appengine.tools.mapreduce.MapOnlyMapper

import me.waitlist.stats.listener.ContextListener
import me.waitlist.core.dao.DailyStatsDao
import me.waitlist.core.dao.HourlyStatsDao
import me.waitlist.core.dao.OverallStatsDao
import me.waitlist.core.dao.crit.EQRawKeyCriterion
import me.waitlist.core.dao.crit.GTECriterion
import me.waitlist.core.db.Place

/**
 * Mapper for on <code>Place</code> <code>Stats</code> calculations.
 *
 * @author dsumera
 */
class PlaceMapper extends MapOnlyMapper[Key, Place] with Serializable {

  // need to be loaded within the mapper
  @transient private[this] var dlyDao: DailyStatsDao = _
  @transient private[this] var hrDao: HourlyStatsDao = _
  @transient private[this] var ovrDao: OverallStatsDao = _

  override def beginSlice {
    dlyDao = ContextListener.injector.getInstance(classOf[DailyStatsDao])
    hrDao = ContextListener.injector.getInstance(classOf[HourlyStatsDao])
    ovrDao = ContextListener.injector.getInstance(classOf[OverallStatsDao])
    super.beginSlice
  }

  override def map(k: Key) = {
    // place container, be sure to set the id
    val place = new Place
    place.id = k.getId

    // calculate date cutoffs using calendar
    val cal = Calendar.getInstance

    // emit hourly stats for the past day
    cal.add(Calendar.DAY_OF_MONTH, -1)
    hrDao.find(EQRawKeyCriterion("place", k), GTECriterion("date", cal.getTime))()._1 match {
      case Nil => // no hourly stats for past day

      // process all hourly stats for past day
      case stats => place.seated_last_day = stats.map(_.total_seated).sum
    }

    // emit daily stats for the past week
    cal.add(Calendar.DAY_OF_MONTH, -6)
    dlyDao.find(EQRawKeyCriterion("place", k), GTECriterion("date", cal.getTime))()._1 match {
      case Nil => // no daily stats for past week

      // process all daily stats for past week
      case stats => place.seated_last_week = stats.map(_.total_seated).sum
    }

    // emit daily stats for the past month
    cal.add(Calendar.DAY_OF_MONTH, 7)
    cal.add(Calendar.MONTH, -1)
    dlyDao.find(EQRawKeyCriterion("place", k), GTECriterion("date", cal.getTime))()._1 match {
      case Nil => // no daily stats for past week

      // process all daily stats for past month
      case stats => place.seated_last_month = stats.map(_.total_seated).sum
    }

    // emit overall stats
    ovrDao.find(EQRawKeyCriterion("place", k))()._1 match {
      case Nil => // no overall stat for place

      case stat :: _ => place.total_seated = stat.total_seated
    }

    emit(place)
  }
}