package me.waitlist.stats
package mapreduce.mappers

import java.io.Serializable
import java.util.Calendar
import java.util.Date
import java.util.TimeZone

import com.google.appengine.api.datastore.Entity
import com.google.appengine.tools.mapreduce.Mapper

import me.waitlist.core.dao.HourlyStatsDao
import me.waitlist.core.dao.UserDao
import me.waitlist.core.dao.crit.EQCriterion
import me.waitlist.core.dao.crit.GTCriterion
import me.waitlist.core.dao.crit.LTECriterion
import me.waitlist.core.db.HourlyStats
import me.waitlist.core.traits.Dateable
import me.waitlist.stats.listener.ContextListener

/**
 * Mapper for <code>Stats</code> calculations.
 *
 * @author dsumera
 */
class StatsMapper(offset: Int) extends Mapper[Entity, String, HourlyStats]
    with Dateable
    with Serializable {

  // need to be loaded within the mapper
  @transient private[this] var hrDao: HourlyStatsDao = _
  @transient private[this] var userDao: UserDao = _

  override def beginSlice {
    hrDao = ContextListener.injector.getInstance(classOf[HourlyStatsDao])
    userDao = ContextListener.injector.getInstance(classOf[UserDao])
    super.beginSlice
  }

  override def map(e: Entity) = {
    // emit each hourly stat value for key:
    //    all-time place stat
    //    all-time overall stat (place-all)
    //    hourly overall stat (place-all)
    //    daily overall stat (place-all) - daily is based up until 5am next morning
    //    daily place stat - daily is based up until 5am next morning

    // 1) retrieve associated place
    Option(e.getProperty("place").asInstanceOf[com.google.appengine.api.datastore.Key]) match {
      case None => // overall object has no place, ignore

      // 2) check if overall stat updated within offset hours
      case Some(place) =>

        // offset from current hour (zero out fields up to hour)
        val cal = Calendar.getInstance
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        
        // go back offset hours
        cal.add(Calendar.HOUR_OF_DAY, offset)

        // offset a month to create before date
        cal.add(Calendar.MONTH, -1)
        val updated = Option(e.getProperty("last_updated").asInstanceOf[Date]) match {
          // use before date for last updated
          case None => cal.getTime
          case Some(date) => date
        }
        // set to original cutoff
        cal.add(Calendar.MONTH, 1)

        val cutoff = cal.getTime
        cutoff after updated match {
          case false => // overall updated w/in past offset hours, continue

          // 3) use place to find all hourly stats from last updated to offset hours ago
          case true => hrDao.find(EQCriterion("place", place), GTCriterion("date", updated), LTECriterion("date", cutoff))()._1 match {
            case Nil => // no hourly stats

            // 4) for each hourly stat emit appropriate place and cumulative stats
            case stats => for (stat <- stats) {
              // overall cumulative, emit overall stat (create OverallStat key)
              emit("o:place-all", stat)

              // overall place, emit overall place stat (create OverallStat key for place)
              val placeId = place.getId
              emit(s"o:place-${placeId}", stat)

              // all ids should be formatted to UTC
              val fmt = getSimpleFormat
              fmt.setTimeZone(Dateable.UTC)

              // stat date is already zero'd out to hour
              cal.setTime(stat.date)

              // hourly cumulative, emit hourly overall stat (create HourlyStat key)
              emit(s"h:place-all|${fmt.format(cal.getTime)}", stat)

              // localize the stat time; timezone is based off of user timezone, use Pacific otherwise
              // fyi, user is also accessible via the UserPlace table
              cal.setTimeZone(TimeZone.getTimeZone(userDao.find(EQCriterion("place_ids", placeId))()._1 match {
                case u :: _ => u.timezone
                case Nil => "US/Pacific"
              }))

              // determine daily cutoff
              // sync to the previous day if before 5:00 am in local timezone of current day
              if (cal.get(Calendar.HOUR_OF_DAY) < 5) cal.add(Calendar.DAY_OF_MONTH, -1)

              // daily stats, reset localization to UTC before zeroing the hour
              cal.setTimeZone(Dateable.UTC)
              cal.set(Calendar.HOUR_OF_DAY, 0)
              val daily = fmt.format(cal.getTime)

              // daily cumulative, emit daily overall stat (create DailyStat key) - wrt timezone,
              // day will be current day up to 5am next day
              emit(s"d:place-all|${daily}", stat)

              // daily place, emit daily place stat (create DailyStat key for place) - wrt timezone,
              // day will be current day up to 5am next day
              emit(s"d:place-${placeId}|${daily}", stat)
            }
          }
        }
    }
  }
}