package me.waitlist.stats
package mapreduce.reducers

import scala.collection.JavaConversions.asScalaIterator
import com.google.appengine.tools.mapreduce.Reducer
import com.google.appengine.tools.mapreduce.ReducerInput
import me.waitlist.core.db.HourlyStats

/**
 * <code>Reducer</code> for <code>HourlyStats</code> objects.
 *
 * @author dsumera
 */
class StatsReducer extends Reducer[String, HourlyStats, HourlyStats] {
  override def reduce(key: String, values: ReducerInput[HourlyStats]) {

    // prepare stats data transfer object
    val rv = new HourlyStats

    // needs to be parsed on output step, combines stat type with id
    rv.id = key

    // sort stats by date descending
    val stats = values.toList.sortWith((a, b) => a.date.after(b.date))

    // date
    rv.date = stats(0).date
    
    // place
    rv.place = stats(0).place

    // perform cumulative operations
    for (stat <- stats) rv merge stat

    // send to stat output
    emit(rv)
  }
}