//package me.waitlist.stats
//package http
//
//import java.text.SimpleDateFormat
//import java.util.Calendar
//import java.util.TimeZone
//
//import com.googlecode.objectify.Ref
//
//import javax.inject.Inject
//import javax.inject.Singleton
//import javax.servlet.http.HttpServlet
//import javax.servlet.http.HttpServletRequest
//import javax.servlet.http.HttpServletResponse
//import me.waitlist.core.dao.HourlyStatsDao
//import me.waitlist.core.dao.OverallStatsDao
//import me.waitlist.core.dao.PlaceDao
//import me.waitlist.core.db.HourlyStats
//import me.waitlist.core.db.OverallStats
//import me.waitlist.core.db.Place
//
///**
// * Testing out map-reduce stuff.
// *
// * @author dsumera
// */
//@Singleton
//class TestServlet extends HttpServlet {
//
//  // named variables
//  @Inject private[this] var placeDao: PlaceDao = _
//  @Inject private[this] var hrDao: HourlyStatsDao = _
//  @Inject private[this] var ovrDao: OverallStatsDao = _
//
//  /**
//   * Delete to post.
//   * @param req
//   * @param res
//   */
//  override def doGet(req: HttpServletRequest, res: HttpServletResponse) = doPost(req, res)
//
//  override def doPost(req: HttpServletRequest, res: HttpServletResponse) {
//    val place = new Place
//    place.name = req.getParameter("name")
//    place.name_lower = place.name.toLowerCase
//
//    // save place
//    val place1 = placeDao.persist(place).values.iterator.next
//
//    val stats = new HourlyStats
//
//    // one day ago
//    val c = Calendar.getInstance
//    c.add(Calendar.DAY_OF_MONTH, -1)
//    c.set(Calendar.MINUTE, 0)
//    c.set(Calendar.SECOND, 0)
//    c.set(Calendar.MILLISECOND, 0)
//
//    stats.place = Ref.create(place1)
//    stats.adult = 345
//    stats.old = 14
//    stats.total_seated = 267
//    stats.date = c.getTime
//    val format = new SimpleDateFormat("yyyyMMdd-HHmm")
//    format.setTimeZone(TimeZone.getTimeZone("UTC"))
//
//    // save hourly place stat
//    stats.id = s"place-${place1.id}|${format.format(stats.date)}"
//    hrDao.persist(stats)
//
//    // save overall place stat
//    val ovr = new OverallStats
//    ovr.id = s"place-${place1.id}"
//    // one month ago
//    c.add(Calendar.MONTH, -1)
//    ovr.last_updated = c.getTime
//    ovr.place = stats.place
//    ovrDao.persist(ovr)
//  }
//}