package me.waitlist.stats
package http

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.TimeZone
import javax.inject.Inject
import javax.inject.Singleton
import me.waitlist.core.dao.HourlyStatsDao
import me.waitlist.core.db.HourlyStats
import me.waitlist.core.traits.Dateable

/**
 * Endpoint for modifying <code>HourlyStats</code> information.
 *
 * @author dsumera
 */
@Singleton
class HourlyServlet extends UpdateServlet[HourlyStats] with Dateable {

  @Inject private[this] var hrDao: HourlyStatsDao = _

  override def getStat(place: Long) = {
    // offset from current hour (zero out fields up to hour)
    val cal = Calendar.getInstance
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    val date = cal.getTime

    // all ids should be formatted to UTC
    val fmt = getSimpleFormat
    fmt.setTimeZone(Dateable.UTC)

    // find and/or create the hourly stats object and return
    val stat = hrDao.findOrCreate(s"place-${place}|${fmt.format(date)}")
    stat.date = date
    stat
  }

  override def saveStat(stat: HourlyStats) = hrDao.persist(stat)
}