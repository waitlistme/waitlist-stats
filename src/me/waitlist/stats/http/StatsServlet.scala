package me.waitlist.stats
package http

import java.util.Date

import com.google.appengine.tools.mapreduce.MapJob
import com.google.appengine.tools.mapreduce.MapReduceJob
import com.google.appengine.tools.mapreduce.MapReduceSettings
import com.google.appengine.tools.mapreduce.MapReduceSpecification
import com.google.appengine.tools.mapreduce.MapSettings
import com.google.appengine.tools.mapreduce.MapSpecification
import com.google.appengine.tools.mapreduce.Marshallers
import com.google.appengine.tools.mapreduce.inputs.DatastoreInput
import com.google.appengine.tools.mapreduce.inputs.DatastoreKeyInput

import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton
import me.waitlist.core.db.HourlyStats
import me.waitlist.core.http.DispatchServlet
import me.waitlist.core.traits.Dateable
import me.waitlist.stats.mapreduce.mappers.PlaceMapper
import me.waitlist.stats.mapreduce.mappers.StatsMapper
import me.waitlist.stats.mapreduce.outputs.PlaceOutput
import me.waitlist.stats.mapreduce.outputs.StatsOutput
import me.waitlist.stats.mapreduce.reducers.StatsReducer

/**
 * Calculate <code>Stats</code>-based entities for metrics.
 *
 * /cron/stats?all - writes stats against hourly, daily and overall categories
 * /cron/stats?place - writes stats against <code>Place</code> entities
 *
 * @author dsumera
 */
@Singleton
class StatsServlet extends DispatchServlet with Dateable {

  // mapreduce operational parameters set in ServerModule.scala
  @Inject @Named("stats_shards") private[this] var shards: Int = _
  @Inject @Named("stats_reducers") private[this] var reducers: Int = _
  @Inject @Named("stats_offset") private[this] var offset: Int = _
  @Inject @Named("queue") private[this] var queue: String = _
  @Inject @Named("bucket") private[this] var bucket: String = _

  // use the path info to determine what type of stats computation to perform
  override def dispatch = {
    // update [on] place statistics (mapper-only job)
    case "place" => MapJob start (
      // specification
      new MapSpecification.Builder(new DatastoreKeyInput("Place", shards),
        new PlaceMapper, new PlaceOutput)
      // human-readable name
      .setJobName(s"stats-place-${getSimpleFormat.format(new Date)}")
      .build(),

      // settings
      new MapSettings.Builder()
      .setModule("stats")
      .setWorkerQueueName(queue)
      .build())

    // update all stats
    case "all" => MapReduceJob start (
      // specification
      new MapReduceSpecification.Builder(new DatastoreInput("OverallStats", shards),
        new StatsMapper(offset), new StatsReducer, new StatsOutput)
      .setKeyMarshaller(Marshallers.getStringMarshaller)
      .setValueMarshaller(Marshallers.getSerializationMarshaller[HourlyStats])
      .setNumReducers(reducers)
      // human-readable name
      .setJobName(s"stats-all-${getSimpleFormat.format(new Date)}")
      .build(),

      // settings
      new MapReduceSettings.Builder()
      // module on which mapreduce will run
      .setModule("stats")
      // queue on which tasks will be pushed
      .setWorkerQueueName(queue)
      // gcs bucket for mapreduce
      .setBucketName(bucket)
      .build())

    // send to the index by default
    //req.getRequestDispatcher("/index.html").forward(req, res)

    //    //String queue = getStringParam(req, "queue", "mapreduce-workers");
    //    //String module = getStringParam(req, "module", "mapreduce");
    //    //String bucket = getBucketParam(req);
    //    //long start = getLongParam(req, "start", 0);
    //    //long limit = getLongParam(req, "limit", 3 * 1000 * 1000);
    //    //int shards = Math.max(1, Math.min(100, Ints.saturatedCast(getLongParam(req, "shards", 10))));
    //    // [START start_mapreduce]
    //    //val spec 
    //
    //    BigQuery
    //    Output<ArrayList<Integer>, GoogleCloudStorageFileSet> output = new MarshallingOutput<>(
    //      new GoogleCloudStorageFileOutput(bucket, "CollidingSeeds-%04d", "integers"),
    //      outputMarshaller);
    //    // [START mapReduceSpec]
    //    MapReduceSpecification<Long, Integer, Integer, ArrayList<Integer>, GoogleCloudStorageFileSet>
    //      spec = new MapReduceSpecification.Builder<>(input, mapper, reducer, output)
    //      .setKeyMarshaller(intermediateKeyMarshaller)
    //      .setValueMarshaller(intermediateValueMarshaller)
    //      .setJobName("DemoMapreduce")
    //      .setNumReducers(shards)
    //      .build();
    //    // [END mapReduceSpec]
    //    return spec;
    //  }
    //  // [END createMapReduceSpec]
    //
    //  // [START getSettings]
    //  public static MapReduceSettings getSettings(String bucket, String queue, String module) {
    //    // [START mapReduceSettings]
    //    MapReduceSettings settings = new MapReduceSettings.Builder()
    //        .setBucketName(bucket)
    //        .setWorkerQueueName(queue)
    //        .setModule(module) // if queue is null will use the current queue or "default" if none
    //        .build();
    //    // [END mapReduceSettings]
    //    return settings;
    //
    //
    ////    MapReduceSpecification < Long, Integer, Integer, ArrayList < Integer >, GoogleCloudStorageFileSet >
    ////      mapReduceSpec = createMapReduceSpec(bucket, start, limit, shards);
    ////    MapReduceSettings settings = getSettings(bucket, queue, module);
    ////    // [START startMapReduceJob]
    ////    String id = MapReduceJob.start(mapReduceSpec, settings);
    ////    // [END startMapReduceJob]
    ////    // [END start_mapreduce]
    //    resp.sendRedirect("/_ah/pipeline/status.html?root=" + id);
  }
}