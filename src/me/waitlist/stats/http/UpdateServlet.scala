package me.waitlist.stats
package http

import scala.collection.JavaConversions._
import com.googlecode.objectify.Key
import com.googlecode.objectify.Ref
import javax.inject.Inject
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import me.waitlist.core.db.Place
import me.waitlist.core.db.Stats
import java.util.logging.Logger

/**
 * Servlet which updates <code>Stats</code> entities.
 */
abstract class UpdateServlet[T <: Stats] extends HttpServlet {

  @Inject private[this] var log: Logger = _
  
  override def doGet(req: HttpServletRequest, res: HttpServletResponse) = doPost(req, res)

  override def doPost(req: HttpServletRequest, res: HttpServletResponse) = Option(req.getParameter("place")) match {
    // do nothing, there is no place provided
    case None => log.warning("place id is required")

    // update the statistics for the given place
    case Some(place) =>
      
      val id = place.trim.toLong
      
      // retrieve stats object
      val stat = getStat(id)
      
      // assign stats place
      stat.place = Ref.create(Key.create(classOf[Place], id))

      // compute stats according to provided parameters
      for ((k, v: Array[String]) <- req.getParameterMap) k match {
        case "waitlist_add" => stat.waitlist_add += v.head.toInt
        case "waitlist_remove" => stat.waitlist_remove += v.head.toInt
        case _ => // do nothing
      }
      
      // update stats
      saveStat(stat)
  }

  /**
   * Retrive the <code>Stats</code> object given the place id.
   */
  def getStat(place: Long): T
  
  /**
   * Save the <code>Stats</code> object.
   */
  def saveStat(stats: T)
}