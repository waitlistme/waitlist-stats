# README

waitlist.me app engine "stats" module

### Purpose

* Uses mapreduce on `stats` task queue to calculate stats.

### Configuration

1. In `default` module `queue.yaml/queue.xml`, define `stats` queue which targets `stats` module. This ensures that any tasks queued on `stats` will run on the `stats` module.
1. In `default` module `cron.yaml/cron.xml`, define two separate jobs `cron\stats\all` and `cron\stats\place` (i.e., webhooks) targeted at `stats` module. This ensures that these cron tasks run specifically on the `stats` module.
1. On `Google Cloud Storage`, create a bucket for staging phases of stats mapreduce jobs. All map, shuffle and reduce intermediate results are persisted through cloud storage before being written to the datastore during the output stage.
1. Set the number of `shards`, `reducers`, `stats offset`, `queue` and `bucket` in `ServerModule.scala` file.

example of if you just wanna do code

	val x = 2
	x + 1
	println(x)

### How do I get set up?

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines

* Writing tests
* Code review
* Other guidelines
* [stats module wiki](https://bitbucket.org/waitlistme/waitlist-stats/wiki/Home)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)